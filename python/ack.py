import sys
import logging

def ack(m,n):
    if m == 0:
        return n + 1
    elif m > 0 and n == 0:
        return ack(m - 1, 1)
    elif m > 0 and n > 0:
        return ack(m - 1, ack(m, n - 1))
    else:
        print "both 'm' and 'n' should be bigger than 0 (m,n) = " + str((m,n))
        return None

def parse_line(line):
    split = line.split('=')
    args = split[0].split(',')
    m = int(args[0])
    n = int(args[1])
    expected_result = int(split[1].rstrip('\n'))
    return (m,n,expected_result)

def test(files):
    TESTS_FOLDER = "../tests/"
    for file in files:
        TEST_FILE = TESTS_FOLDER + str(file)
        logging.info("testing with " + TEST_FILE)
        file = open(TEST_FILE, 'r')
        for line in file:
            (m,n, expected_result) = parse_line(line)

            #time start
            time = None
            result = ack(m,n)
            #end time

            assert expected_result == result

            logging.info("test " + str((m,n)) + " took " + str(time) + " with result = " + str(result))

logging.basicConfig(level=logging.NOTSET,
                    format='%(asctime)s %(levelname)s %(message)s')
#sys.setrecursionlimit(10000)
test(sys.argv[1:]) #discard argv[0] because it's the name of this python file
