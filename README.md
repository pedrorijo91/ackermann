# ackermann
Speed comparison of several programming languages using Ackermann function.

Note that this tests are more like a simple play for fun and to use a bit each of the chosen languages than a universal truth regarding language performance issues. In some cases I may not even using the best language features :)

## Usage

You can use the [test script](test_all.sh) or you can run each language individually.

* [C](c/test_c.sh)
* [Clojure](clojure/test_clojure.sh)
* [Haskell](haskell/test_haskell.sh)
* [Java](java/test_java.sh)
* [Lisp](lisp/test_lisp.sh)
* [Prolog](prolog/test_prolog.sh)
* [Python](python/test_python.sh)
* [Ruby](ruby/test_ruby.sh)
* [Scala](scala/test_scala.sh)

> Note: You may need to get the proper software in order to run the tests

## About the Ackermann's function:

In computability theory, the [**Ackermann function**](http://en.wikipedia.org/wiki/Ackermann_function), named after [Wilhelm Ackermann](http://en.wikipedia.org/wiki/Wilhelm_Ackermann), is one of the simplest and earliest-discovered examples of a total computable function that is not primitive recursive. All primitive recursive functions are total and computable, but the Ackermann function illustrates that not all total computable functions are primitive recursive.

After Ackermann's publication of his function (which had three nonnegative integer arguments), many authors modified it to suit various purposes, so that today "the Ackermann function" may refer to any of numerous variants of the original function. One common version, the two-argument **Ackermann–Péter function**, is defined as follows for nonnegative integers m and n:

![equation](http://www.sciweavers.org/tex2img.php?eq=ackermann%28m%2C%20n%29%20%3D%0A%5Cbegin%7Bcases%7D%0An%20%2B%201%20%26%20m%20%3D%200%20%5C%5C%0Aackermann%28m-1%2C%201%29%20%26%20m%20%3E%200%2C%20n%20%3D%200%20%5C%5C%0Aackermann%28m%20-%201%2C%20ackermann%28m%2C%20n%20-%201%29%29%20%26%20m%20%3E%200%2C%20n%20%3E%200%0A%5Cend%7Bcases%7D%20&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0)
