import scala.io.Source

object Ack {

  val TestsFolder = "../tests/"

  def ack(m: Int, n: Int): Int = {
    (m,n) match {
      case (0, _) => n + 1
      case (_, 0) => ack(m - 1, 1)
      case _ => ack(m - 1, ack(m, n - 1))
    }
  }

  def parseLine(line: String): (Int, Int, Int) = {
    val pattern = "([0-9]+),([0-9]+)=([0-9]+)".r
    val pattern(m, n, res) = line
    (m.toInt,n.toInt,res.toInt)
  }

  def main(args: Array[String]) {

    println("starting ack.scala")

    args.foreach(filename => {
      val testFile = TestsFolder + filename
      println("testFile: " + testFile)
      for (line <- Source.fromFile(testFile).getLines()) {
        val (m,n,expectedResult) = parseLine(line)

        val start = System.currentTimeMillis
        val result = ack(m,n)
        val end = System.currentTimeMillis
        val time = end - start

        assert(expectedResult == result, "expected result and actual result differ")

        println("test " + (m,n) + " took " + time + " with result = " + result)
      }
    })

    println("ending ack.scala")

  }
}
